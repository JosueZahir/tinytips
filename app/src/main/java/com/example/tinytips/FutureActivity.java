package com.example.tinytips;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FutureActivity extends AppCompatActivity {
    Button Ejecutar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_future);
        Ejecutar = (Button) findViewById(R.id.imageView38);
        Ejecutar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EnviarDato();
            }
        });
    }

    public void Menu(View v)
    {
        Intent menu = new Intent(this, MainActivity.class);
        startActivity(menu);
    }

    public void test(View v)
    {
        Intent test = new Intent(this, TestActivity.class);
        startActivity(test);
    }
    public void EnviarDato()
    {
        Intent test = new Intent(this, TestActivity.class);
        test.putExtra("TestPast", 4);
        startActivity(test);
    }
}