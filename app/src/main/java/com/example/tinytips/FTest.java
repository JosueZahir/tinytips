package com.example.tinytips;

import android.widget.RadioButton;
import android.widget.TextView;

import org.w3c.dom.Text;

public class FTest {

    public String TipoTest;
    public TextView NombreTema;
    public TextView Pregunta1;
    public TextView Pregunta2;
    public TextView Pregunta3;
    public TextView Pregunta4;
    public TextView Pregunta5;
    public TextView Pregunta6;
    public int res;

    public FTest(TextView NombreTema, TextView Pregunta1, TextView Pregunta2, TextView Pregunta3, TextView Pregunta4, TextView Pregunta5, TextView Pregunta6)
    {
        //this.id_respuestas = id_respuestas;
        this.NombreTema = NombreTema;
        this.Pregunta1 = Pregunta1;
        this.Pregunta2 = Pregunta2;
        this.Pregunta3 = Pregunta3;
        this.Pregunta4 = Pregunta4;
        this.Pregunta5 = Pregunta5;
        this.Pregunta6 = Pregunta6;
    }

    public void PastSimple()
    {
        String PreguntasPS[] = {"1. My classmate and I... our teacher a question about the test","2. John wanted to … work yesterday but he was sick",
                "3. They … to the park because they were very tired.", "4.(A) Did you talk to your boss?, (B) Yes, I …", "5. He … twenty minutes for the bus yesterday.",
                "6. (A) … they fix their bicycles?, (B) Yes, they …"} ;

        NombreTema.setText("Past Simple");
        Pregunta1.setText(PreguntasPS[0]);
        Pregunta2.setText(PreguntasPS[1]);
        Pregunta3.setText(PreguntasPS[2]);
        Pregunta4.setText(PreguntasPS[3]);
        Pregunta5.setText(PreguntasPS[4]);
        Pregunta6.setText(PreguntasPS[5]);

    }

    public void PresentSimple()
    {
        String PreguntasPresentS[] = {"1 What is the auxiliary for the present simple for the third person?", "What is the auxiliary for the present simple for the negative form?",
        "3 What is the use of present simple?", "4 What is the auxiliary for the present simple for I,We,You,they?", "5 What is the sentence correct in present simple?", "6 What is the correct form to create a sentence in present simple?"
        };
        NombreTema.setText("Present Simple");
        Pregunta1.setText(PreguntasPresentS[0]);
        Pregunta2.setText(PreguntasPresentS[1]);
        Pregunta3.setText(PreguntasPresentS[2]);
        Pregunta4.setText(PreguntasPresentS[3]);
        Pregunta5.setText(PreguntasPresentS[4]);
        Pregunta6.setText(PreguntasPresentS[5]);
    }
    public void PresentCo()
    {
        String PreguntasPC[] = {"1 What is the use of present continuous?", "2 What are the signals words in present continuous?",
        "3 What is the implement form for the verb in present continuous?", " 4What is the correct form to create a questions in present continuous?",
        "5 What is the sentence correct in present cotinuous?", "6 What is the sentence correct in present cotinuous(NEGATIVE)?"};
        NombreTema.setText("Present Continous");
        Pregunta1.setText(PreguntasPC[0]);
        Pregunta2.setText(PreguntasPC[1]);
        Pregunta3.setText(PreguntasPC[2]);
        Pregunta4.setText(PreguntasPC[3]);
        Pregunta5.setText(PreguntasPC[4]);
        Pregunta6.setText(PreguntasPC[5]);

    }
    public void Future()
    {
        String PreguntasF[]={"1 What is the use of future?", "2 What are the auxiliaries more common for the future?",
                "3 What is the difference between will and going to?","4 What is the use of Going to?", "5 What is the use of Will?",
        "6 What is the sentence correct in future?"};
        NombreTema.setText("Future");
        Pregunta1.setText(PreguntasF[0]);
        Pregunta2.setText(PreguntasF[1]);
        Pregunta3.setText(PreguntasF[2]);
        Pregunta4.setText(PreguntasF[3]);
        Pregunta5.setText(PreguntasF[4]);
        Pregunta6.setText(PreguntasF[5]);
    }

    public void Questions()
    {
        String Questionss[]={"1 What's the use of  the word \"who\" in a question?", "2 What's the use of  the word \"where\" in a question?",
        "3 What's the use of  the word \"when\" in a question?", "4 What's the use of  the word \"why\" in a question?", "5 What's the use of  the word 'which and what' in a question?"};
        NombreTema.setText("Questions");
        Pregunta1.setText(Questionss[0]);
        Pregunta2.setText(Questionss[1]);
        Pregunta3.setText(Questionss[2]);
        Pregunta4.setText(Questionss[3]);
        Pregunta5.setText(Questionss[4]);
        Pregunta6.setText(Questionss[5]);
    }

    public void TPerson()
    {
        String TPersonQ[] = {"1 What are the subject pronoun for third person?", "What is the letter that should add in verbs in third person?",
        "3 What are the letters that should add in verbs ending in ss, sh, ch, x and o in third person?", "4 What are the letters that should add in verbs ending in consonant +  y in third person?",
        "5 What is the verb to be for the third person?", "6 Which is the form possessive for the third person?"};
        NombreTema.setText("Third Person");
        Pregunta1.setText(TPersonQ[0]);
        Pregunta2.setText(TPersonQ[1]);
        Pregunta3.setText(TPersonQ[2]);
        Pregunta4.setText(TPersonQ[3]);
        Pregunta5.setText(TPersonQ[4]);
        Pregunta6.setText(TPersonQ[5]);
    }

    public void Vocabulary()
    {
        String VocabularyT[] = {"1 What's the meaning of the word Just?", "2 What's the meaning of the word Look?", "3 What's the meaning of the word Come?",
        "4 What's the meaning of the word Also?", "5 What's the meaning of the word Dispel?", "6 What's the meaning of the word Family?"};
        NombreTema.setText("Vocabulary");
        Pregunta1.setText(VocabularyT[0]);
        Pregunta2.setText(VocabularyT[1]);
        Pregunta3.setText(VocabularyT[2]);
        Pregunta4.setText(VocabularyT[3]);
        Pregunta5.setText(VocabularyT[4]);
        Pregunta6.setText(VocabularyT[5]);
    }


}
