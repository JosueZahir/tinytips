package com.example.tinytips;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    //metodo de los botones
   public void Pasado(View v)
   {
       Intent pas = new Intent(this, PastActivity.class);
       startActivity(pas);
   }
    public void Presents(View v)
    {
        Intent pres = new Intent(this, PresentsActivity.class);
        startActivity(pres);
    }
    public void Presentc(View v)
    {
        Intent prec = new Intent(this, PresentcActivity.class);
        startActivity(prec);
    }
    public void Futuro(View v)
    {
        Intent fu = new Intent(this, FutureActivity.class);
        startActivity(fu);
    }
    public void Question(View v)
    {
        Intent que = new Intent(this, QuestionsActivity.class);
        startActivity(que);
    }
    public void Voca(View v)
    {
        Intent voc = new Intent(this, VocabularyActivity.class);
        startActivity(voc);
    }
    public void Third(View v)
    {
        Intent th = new Intent(this, ThirdpActivity.class);
        startActivity(th);
    }

    public void Btnsig(View v)
    {
        Intent Sig = new Intent(this, PastActivity.class);
        startActivity(Sig);
    }

}