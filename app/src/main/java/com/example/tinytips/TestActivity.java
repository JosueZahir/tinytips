package com.example.tinytips;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

public class TestActivity extends AppCompatActivity {
    public int id_respuestas [] = {
            R.id.rbAnswer1, R.id.rbAnswer2, R.id.rbAnswer3, R.id.rbAnswer4, R.id.rbAnswer5, R.id.rbAnswer6,
            R.id.rbAnswer7, R.id.rbAnswer8, R.id.rbAnswer9, R.id.rbAnswer10, R.id.rbAnswer10, R.id.rbAnswer11,
            R.id.rbAnswer12, R.id.rbAnswer13, R.id.rbAnswer14, R.id.rbAnswer15, R.id.rbAnswer16, R.id.rbAnswer17,
            R.id.rbAnswer18
    };


    int dato;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);


        Bundle bundle = this.getIntent().getExtras();
        dato = bundle.getInt("TestPast");


        TextView Tema = (TextView) findViewById(R.id.textViewTema);
        TextView Question1 = (TextView) findViewById(R.id.textViewQue1);
        TextView Question2 = (TextView) findViewById(R.id.textViewQue2);
        TextView Question3 = (TextView) findViewById(R.id.textViewQue3);
        TextView Question4 = (TextView) findViewById(R.id.textViewQue4);
        TextView Question5 = (TextView) findViewById(R.id.textViewQue5);
        TextView Question6 = (TextView) findViewById(R.id.textViewQue6);




        FTest nt = new FTest(Tema,Question1, Question2, Question3, Question4, Question5, Question6);
        String[] RespuestasT1 = getResources().getStringArray(R.array.RespuestasT1);
        String[] RespuestasT2 = getResources().getStringArray(R.array.RespuestasT2);
        String[] RespuestasT3 = getResources().getStringArray(R.array.RespuestasT3);
        String[] RespuestasT4 = getResources().getStringArray(R.array.RespuestasT4);
        String[] RespuestasT5 = getResources().getStringArray(R.array.RespuestasT5);
        String[] RespuestasT6 = getResources().getStringArray(R.array.RespuestasT6);
        String[] RespuestasT7 = getResources().getStringArray(R.array.RespuestasT7);

        if (dato == 1)
        {

            nt.PastSimple();
            for (int i=0; i<id_respuestas.length-1; i++)
            {
                RadioButton rb = (RadioButton) findViewById(id_respuestas[i]);
                rb.setText(RespuestasT1[i]);

            }

        }else if(dato ==2)
        {
            nt.PresentSimple();
            for (int i=0; i<id_respuestas.length-1; i++)
            {
                RadioButton rb = (RadioButton) findViewById(id_respuestas[i]);
                rb.setText(RespuestasT2[i]);

            }
        } else if (dato == 3)
        {
            nt.PresentCo();
            for (int i=0; i<id_respuestas.length-1; i++)
            {
                RadioButton rb = (RadioButton) findViewById(id_respuestas[i]);
                rb.setText(RespuestasT3[i]);

            }
        } else if (dato==4)
        {
            nt.Future();
            for (int i=0; i<id_respuestas.length-1; i++)
            {
                RadioButton rb = (RadioButton) findViewById(id_respuestas[i]);
                rb.setText(RespuestasT4[i]);

            }
        }else if (dato == 5)
        {
            nt.Questions();;
            for (int i=0; i<id_respuestas.length-1; i++)
            {
                RadioButton rb = (RadioButton) findViewById(id_respuestas[i]);
                rb.setText(RespuestasT5[i]);

            }
        }
        else if (dato == 6)
        {
            nt.Vocabulary();
            for (int i=0; i<id_respuestas.length-1; i++)
            {
                RadioButton rb = (RadioButton) findViewById(id_respuestas[i]);
                rb.setText(RespuestasT6[i]);

            }
        }
        else if (dato ==7)
        {
                nt.TPerson();
                for (int i=0; i<id_respuestas.length-1; i++)
                {
                    RadioButton rb = (RadioButton) findViewById(id_respuestas[i]);
                    rb.setText(RespuestasT7[i]);

                }
        }
    }

    public void Menu(View v)
    {
        Intent menu = new Intent(this, MainActivity.class);
        startActivity(menu);
    }

}